//
//  WebViewController.swift
//  IMERemmittance
//
//  Created by Fm-user on 8/15/18.
//  Copyright © 2018 fusemachines. All rights reserved.
//  @Modified by Manoj Karki - Swift Technology

import UIKit
import WebKit
import Alamofire

class WebViewController: UIViewController, WKUIDelegate, WKScriptMessageHandler {
   
    let reachabilityManager = NetworkReachabilityManager()

    @IBOutlet weak var webViewContainer: UIView!
    
    var webView: WKWebView!
    
    override func loadView() {
      
        let webConfiguration = WKWebViewConfiguration()
        
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
      
    }
    
    override var prefersStatusBarHidden: Bool {
        if #available(iOS 11, *) {
           return false
        }else {
            return true
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        webView.navigationDelegate = self
        webView.uiDelegate = self
        webView.scrollView.bounces = false
        webView.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal
        urlCall()
       
        webView.configuration.userContentController.add(self, name: "downloadHandler")
        webView.configuration.userContentController.add(self, name: "xlsDownloadHandler")
        webView.configuration.userContentController.add(self, name: "phoneDialerHandler")
        
        /// to remove keyboard tool menu (web is your UIWebView)
        webView.evaluateJavaScript("document.activeElement.blur()", completionHandler: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
            UIApplication.shared.statusBArView?.backgroundColor = UIColor(red:0.93, green:0.11, blue:0.14, alpha:1.0)
    }
    
    func isReachableViaWWAN() -> Bool{
        return (reachabilityManager?.isReachableOnWWAN)!
    }
    
    func urlCall(){
        reachabilityManager?.startListening()
        reachabilityManager?.listener = { _ in
            
                if let isNetworkReachable = self.reachabilityManager?.isReachable,
                    isNetworkReachable {
            
//                    let url = URL(string: "https://mobilemerchant.imepay.com.np")!
//                    let url = URL(string: "http://192.168.1.50:9090")!
                    let url = URL(string: "https://mobilemerchant-uat.imepay.com.np")!
//                    let url = URL(string: "http://172.16.4.39:5000")!
                    
                    var urlRequest = URLRequest(url: url)
                    urlRequest.timeoutInterval = 10

                    
                    self.webView.load(urlRequest)
                    
                    Alamofire.request(urlRequest)
                        .responseJSON { response in
                            
                            if let error = response.result.error {
                                
                                if error._code == NSURLErrorTimedOut {
                                    self.alertMessageInfo(title: "Time Out!", message: "Connection Time Out")
                                    
                                } else {
                                    self.webView.load(urlRequest)
                                }
                            }
                    }
                    
                } else {
                    self.alertMessageInfo(title: "No Network!", message: "Please check out internet connection")
                    
                }
            }
        }
    
    /// web script message handler
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "downloadHandler" {
            let imageBase64EncodedString: String = message.body as! String
            QRCodeDownload(imageBase64: imageBase64EncodedString)
           
        } else if message.name == "xlsDownloadHandler" {
            xlsDownload(url: message.body as! String)
            
        } else if message.name == "phoneDialerHandler" {
            phoneDialer(phone: message.body as! String)
        }
    }
    
    func QRCodeDownload(imageBase64: String){
        if  let dataDecoded : Data = Data(base64Encoded: imageBase64, options: .ignoreUnknownCharacters){
            let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("QR_code" + String(NSDate().timeIntervalSince1970) + ".jpg")
            
            do {
                try dataDecoded.write(to: fileURL, options: .atomic)
                
                print(fileURL,"urllll")
                alertMessageInfo(title: "File downloaded!", message: "QR Code has been successfully downloaded and saved in your Files app.")
            } catch {
                alertMessageInfo(title: "Alert!", message: "Error occur during downloading the file.")
            }
        } else {
            alertMessageInfo(title: "Alert!", message: "Error occur during downloading the file.")
        }
    }
    
    func xlsDownload(url: String) {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL.appendPathComponent("file" + String(NSDate().timeIntervalSince1970) + ".xls")
            return (documentsURL, .createIntermediateDirectories)
        }
        
        Alamofire.download(url, to: destination).responseData { response in
            if response.destinationURL != nil {
                self.alertMessageInfo(title: "File downloaded!", message: "Statement has been successfully downloaded and saved in your Files app.")
            } else {
                self.alertMessageInfo(title: "Alert!", message: "Error occur during downloading the file.")
            }
        }
    }
    
    func phoneDialer(phone: String) {
        let phoneNumber: String = "telprompt://".appendingFormat(phone)
        UIApplication.shared.open(URL(string: phoneNumber)!, options: [:], completionHandler: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
    }
    
    
}

extension WebViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        print("did finish navigation")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("did finish ")
    }

    
}
