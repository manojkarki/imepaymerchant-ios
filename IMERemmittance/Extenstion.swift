//
//  Extenstion.swift
//  IMERemmittance
//
//  Created by FMI-12 on 11/21/18.
//  Copyright © 2018 fusemachines. All rights reserved.
//

import Foundation
import  UIKit

extension UIViewController {
    
    func alertMessageInfo(title: String, message: String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
}
